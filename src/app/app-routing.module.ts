import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PresentacionComponent } from './views/presentacion/presentacion.component';

const routes: Routes = [

  { path: 'perfil', loadChildren: () => import('./views/perfil/perfil.module').then(m => m.PerfilModule) },

  { path: 'ejercicio', loadChildren: () => import('./views/ejercicio/ejercicio.module').then(m => m.EjercicioModule) },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
