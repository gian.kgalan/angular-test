import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EjercicioRoutingModule } from './ejercicio-routing.module';
import { EjercicioComponent } from './ejercicio.component';


@NgModule({
  declarations: [
    EjercicioComponent,
  ],
  imports: [
    CommonModule,
    EjercicioRoutingModule
  ],
})
export class EjercicioModule { }
