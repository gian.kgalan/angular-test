import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ejercicio',
  templateUrl: './ejercicio.component.html',
  styleUrls: ['./ejercicio.component.scss']
})
export class EjercicioComponent implements OnInit  {

  response: string = ''
  numbersArray: Array<number> = []
  isOrdened: boolean = false
  numbersArrayOrdened: Array<number> = []

  constructor() { }

  ngOnInit(): void {
      
  }

  result(cantArray: any, maxNumber: any): void{
    
    if(!Number(cantArray) || !Number(maxNumber)) this.response = 'Por favor escribe un número valido';

    else this.arrayGenerator(cantArray, maxNumber)

  }

  arrayGenerator(cantArray: number, maxNumber: number): void{

    let array: Array<number> = Array.from({length: cantArray}, () => Math.round(Math.random() * maxNumber))

    this.response = `El array generado tiene ${cantArray} números y el número máximo es ${maxNumber}`;
    
    this.numbersArray = array

    this.isOrdened = false

  }

  orderArray(): void{

    if(!this.numbersArray.length) this.response = "Por favor genera un array antes de ordenarlo";
    // clonamos el array y ordenamos el clonado
    this.numbersArrayOrdened = this.numbersArray.slice(0)
    this.numbersArrayOrdened.sort((a,b) => a-b)

    this.isOrdened = true

  }

  clean():void{
    let maxNumber = <HTMLInputElement>document.getElementById('maxNumber')
    maxNumber.value = ''
    let cantArray = <HTMLInputElement>document.getElementById('cantArray')
    cantArray.value = ''
    
    this.response = ''
    this.isOrdened = false
    this.numbersArray = []
    this.numbersArrayOrdened = []
  }


}
