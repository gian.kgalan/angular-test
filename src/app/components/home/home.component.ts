import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() {
  }
  
  displayElement = false 

  ngOnInit(): void {
  }

  showSideBar(show: boolean): void{
    // para validar la apartertura del sidebar cuando se abre desde el navbar y se cierra desde el sidebar
    if(show == false && !this.displayElement) show = true    

    this.displayElement = show
    
  }

}
