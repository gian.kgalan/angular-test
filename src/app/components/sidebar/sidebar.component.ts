import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor() { 
  }
  @Input() textoHijo = "oiga menor";

  @Output() hideSideBar = new EventEmitter<boolean>()

  displaySideBar = false

  ngOnInit(): void {
  }
  hide(): void{
    this.hideSideBar.emit(this.displaySideBar)
  }
}
