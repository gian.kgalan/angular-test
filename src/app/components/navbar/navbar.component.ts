import { Component, Output, OnInit, OnDestroy,  EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {

  constructor() { }

  // textoPadre = 'Hola hij2o'

  show = false

  @Output() displayElement = new EventEmitter<boolean>(); 

  ngOnInit(): void {
  }

  showSideBar(): void{
    this.displayElement.emit(this.show = !this.show)
  }

}
